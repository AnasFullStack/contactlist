// Include gulp
var gulp = require('gulp');
// Define base folders
var src = 'public/';
var dest = 'www/public/';
// Include plugins
var plugins = require("gulp-load-plugins")({
   pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
   replaceString: /\bgulp[\-.]/
});

// Concatenate & Minify JS and bower js main files
gulp.task('script', function() {
   var jsFiles = [src + 'js/*.js'];
   gulp.src(plugins.mainBowerFiles().concat(jsFiles))
      .pipe(plugins.filter('*.js'))
      .pipe(plugins.concat('main.js'))
      .pipe(plugins.uglify())
      .pipe(gulp.dest(dest + 'js'));
});
// Concatenate and minify bower_components css
gulp.task('css', function() {
   var cssFiles = [src + 'css/*'];
   gulp.src(plugins.mainBowerFiles().concat(cssFiles))
      .pipe(plugins.filter('*.css'))
      .pipe(plugins.order([
         'normalize.css',
         '*'
      ]))
      .pipe(plugins.concat('vendor.css'))
      .pipe(plugins.rename({suffix: '.min'}))
      //.pipe(plugins.uglify())
      .pipe(gulp.dest(dest + 'css'));
});


// Compile CSS from Sass files
gulp.task('sass', function() {
   return gulp.src(src + 'scss/style.scss')
      .pipe(plugins.rename({suffix: '.min'}))
      .pipe(plugins.sass({style: 'compressed'}))
      .pipe(gulp.dest(dest + 'css'));
});
gulp.task('images', function() {
   return gulp.src(src + 'images/**/*')
      .pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
      .pipe(gulp.dest(dest + 'img'));
});
// Watch for changes in files
gulp.task('watch', function() {
   // Watch .js files
   gulp.watch(src + 'js/*.js', ['scripts']);
   // Watch .scss files
   gulp.watch(src + 'scss/*.scss', ['sass']);
   // Watch image files
   gulp.watch(src + 'images/**/*', ['images']);
});
gulp.task('injectInHtml', function() {
   var target = gulp.src(src + 'index.html');
   var sources = gulp.src([dest+'**/*.js', dest + '**/*.css'], {read: false});
   return target.pipe(plugins.inject(sources,{relative: false}))
      .pipe(gulp.dest(dest));
});


// Default Task
gulp.task('default', ['script', 'css','sass', 'images', 'watch','injectInHtml']);